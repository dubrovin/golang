package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type ME struct {

}

func readFile(path string) {
	fileHandle, _ := os.Open(path)
	defer fileHandle.Close()
	fileScanner := bufio.NewScanner(fileHandle)

	for fileScanner.Scan() {
		fmt.Println(fileScanner.Text())
	}
}

func readFromStdin() {
	fileScanner := bufio.NewScanner(os.Stdin)

	for fileScanner.Scan() {
		linter := strings.Split(fileScanner.Text(), ":")
		linterFile := strings.TrimSpace(linter[0])
		//linterLine := linter[1]
		//linterMsg := linter[3]
		//fmt.Printf("file: %s\nline: %s\nmsg: %s\n", linterFile, linterLine, linterMsg)
		readFile(linterFile)
	}
}

func main() {
	//readFile("/Users/ruslan/work/src/statisfactor/main.go")
	readFromStdin()
}